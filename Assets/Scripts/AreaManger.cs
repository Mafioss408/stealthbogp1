using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AreaManger : MonoBehaviour
{

    [SerializeField] Transform safeZone;
     public Transform SafeZone
    {
        get { return safeZone; }
    }

    public static AreaManger instance { get; private set; } 


    private void Awake()
    {
        if (instance)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
        }
    }
}
