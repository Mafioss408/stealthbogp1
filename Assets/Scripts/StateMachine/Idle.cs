using System.Collections;
using System.Collections.Generic;
using System.IO.Pipes;
using UnityEngine;
using UnityEngine.AI;

public class Idle : AIState
{
    public Idle(GameObject _npc, NavMeshAgent _agent, Animator _anim, Transform _player, Transform[] _checkPoints)
        : base(_npc, _agent, _anim, _player, _checkPoints)
    {
        name = state.Idle;
    }
    public override void Enter()
    {
        anim.SetBool("isMoving", false);
        base.Enter();
    }
    public override void Update()
    {
        if (CanSeePlayer())
        {
            nextState = new Chase(npc, agent, anim, player, checkPoints);
            stage = Event.Exit;

        }
        else if (Random.Range(0, 100) < 10)
        {
            nextState = new Patrol(npc, agent, anim, player, checkPoints);
            stage = Event.Exit;
        }

        base.Update();

    }

    public override void Exit()
    {
        base.Exit();
    }
}
