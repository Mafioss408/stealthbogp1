using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class Chase : AIState
{
    Player playerComp;

    public Chase(GameObject _npc, NavMeshAgent _agent, Animator _anim, Transform _player, Transform[] _checkPoints)
         : base(_npc, _agent, _anim, _player, _checkPoints)
    {
        name = state.Chase;
        agent.speed = 5f;
        agent.isStopped = false;


    }
    public override void Enter()
    {
        anim.SetBool("isMoving", true);
        anim.speed = 2f;
        playerComp = player.GetComponent<Player>();
        base.Enter();
    }
    public override void Update()
    {

        Persue();

        if (agent.hasPath)
        {
            if (CanAttackPlayer())
            {
                nextState = new Attack(npc, agent, anim, player, checkPoints);
                stage = Event.Exit;
            }
        }
        else if (!CanSeePlayer())
        {
            nextState = new Patrol(npc, agent, anim, player, checkPoints);
            stage = Event.Exit;
        }

        base.Update();
    }

    public override void Exit()
    {
        anim.SetBool("isMoving", false);
        anim.speed = 1f;
        base.Exit();
    }


    void Persue()
    {
        Vector3 targetDir = player.position - npc.transform.position;

        float relativeHeading = Vector3.Angle(npc.transform.forward, npc.transform.TransformVector(playerComp.CurrentVelocity.normalized));
        float toTarget = Vector3.Angle(npc.transform.forward,npc.transform.TransformVector(targetDir));





        float lookHead = targetDir.magnitude / (agent.speed + playerComp.GetComponent<Player>().CurrentVelocity.magnitude);
        Vector3 dest = player.position + playerComp.CurrentVelocity.normalized * lookHead;


        Debug.DrawRay(player.position, playerComp.CurrentVelocity.normalized, Color.green);
        Debug.DrawRay(npc.transform.position, playerComp.CurrentVelocity.normalized * lookHead * 100, Color.magenta);



        agent.SetDestination(dest);
    }

}
