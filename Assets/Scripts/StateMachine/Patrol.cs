using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.AI;

public class Patrol : AIState
{
    int currentIndex = -1;

    public Patrol(GameObject _npc, NavMeshAgent _agent, Animator _anim, Transform _player, Transform[] _checkPoints) : base(_npc, _agent, _anim, _player, _checkPoints)
    {
        name = state.Patrol;
        agent.speed = 2f;
        agent.isStopped = false;

        

    }

    public override void Enter()
    {
       // currentIndex = 0;
       float dist = Mathf.Infinity;

        for (int i = 0; i < checkPoints.Length; i++)
        {
            Transform currentCheckpoints = checkPoints[i];
            float tempDist  = Vector3.Distance(npc.transform.position, currentCheckpoints.transform.position);

            if( tempDist < dist )
            {
                currentIndex = i - 1;
                dist = tempDist;    
            }
        }

        anim.SetBool("isMoving", true);
        base.Enter();

    }
    public override void Update()
    {

        if(agent.remainingDistance < 2)
        {
            if (currentIndex >= checkPoints.Length - 1)
            {
                currentIndex = 0;
            }
            else
                currentIndex++;

            agent.SetDestination(checkPoints[currentIndex].position);
        }

        if (CanSeePlayer())
        {
            nextState = new Chase(npc, agent, anim, player, checkPoints);
            stage = Event.Exit;
        }
        else if (isPlayerBehind())
        {
            nextState = new Flee(npc, agent, anim, player, checkPoints);
            stage = Event.Exit;
        }
    }

    public override void Exit() 
    {
        anim.SetBool("isMoving", false);
        base.Exit();

    }


}
