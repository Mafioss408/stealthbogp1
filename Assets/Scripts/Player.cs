using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(Rigidbody))]
public class Player : MonoBehaviour
{

    [SerializeField] private float movementSpeed;
    [SerializeField] private float stealthMovementSpeed;
    [SerializeField] private float acceleration;
    [SerializeField] private float turnAcceleration;
    [SerializeField] private float turnSpeed;
    [SerializeField] private float jumpHeight;
    [SerializeField] private LayerMask groundMask;

    bool isGrounded;
    bool isStealthMode;
    bool desiredJump;
    // refs
    private Rigidbody rb;
    private Animator anim;
    private Transform cam;
    Vector3 velocity, desiredvelocity;

    public Vector3 CurrentVelocity { get { return velocity; }  }
    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
        anim = transform.GetChild(0).GetComponent<Animator>();
        cam = Camera.main.transform;

    }
    private void Update()
    {
        GroundCheck();
        Move();
        Jump();

        if (Input.GetButtonDown("Crouch"))
        {

            SwitchStealth();

        }
    }

    private void FixedUpdate()
    {
        velocity = rb.velocity;
        float speedChange = acceleration * Time.deltaTime;

        velocity.x = Mathf.MoveTowards(velocity.x, desiredvelocity.x, speedChange);
        velocity.z = Mathf.MoveTowards(velocity.z, desiredvelocity.z, speedChange);

        if (desiredJump)
        {
            velocity.y += Mathf.Sqrt(-2f * Physics.gravity.y * jumpHeight);

            desiredJump = false;
        }

        rb.velocity = velocity;

        isGrounded = false;
    }


    private void Move()
    {
        float xMove = Input.GetAxis("Horizontal");
        float zMove = Input.GetAxis("Vertical");

        anim.SetFloat("Strafe", xMove);
        anim.SetFloat("Throttle", zMove);

        Vector3 direction = (cam.right * xMove + cam.forward * zMove).normalized;

        desiredvelocity = isStealthMode ? direction * stealthMovementSpeed : direction * movementSpeed;

        //if (desiredvelocity.magnitude > 0.1f)
        //{
        //    //float targetAngle = Mathf.Atan2(desiredvelocity.x, desiredvelocity.z) * Mathf.Rad2Deg + cam.eulerAngles.y;
        //    //float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref turnAcceleration, turnSpeed);

        //    transform.rotation = Quaternion.Euler(0f, targetAngle, 0f);

        //}
        
        transform.forward = Vector3.ProjectOnPlane(cam.forward, Vector3.up);
        //velocity.y = rb.velocity.y;
    }

    private void SwitchStealth()
    {

        isStealthMode = !isStealthMode;
        anim.SetBool("Stealth", isStealthMode);

    }

    private void Jump()
    {

        if (!isGrounded) return;
        desiredJump |= Input.GetButtonDown("Jump");



        if (!desiredJump) return;
        anim.SetBool("isJumping", true);


        if (isStealthMode)
            SwitchStealth();


    }

    private void GroundCheck()
    {
        if (Physics.CheckSphere(transform.position, 0.1f, groundMask))
        {
            isGrounded = true;
            anim.SetBool("isJumping", false);
        }
    }


}
