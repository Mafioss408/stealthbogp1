using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


public class SimpleStateMachine : MonoBehaviour
{
    public state actualState = state.Idle;
    NavMeshAgent agent;
    public Transform[] targets;
    Animator anim;


    int actualTarget = 0;
    private void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
        anim = transform.GetChild(0).GetComponent<Animator>();
    }


    private void Update()
    {
        if (actualState == state.Dead) return;



        if (Input.GetKeyDown(KeyCode.Space))
        {
            actualState = state.Patrol;
        }
        else if (Input.GetKeyDown(KeyCode.Escape))
        {
            actualState = state.Chase;
        }


        if (actualState == state.Patrol)
        {
            Debug.Log("Patrolling");
            agent.SetDestination(targets[actualTarget].position);
            if (Vector3.Distance(agent.destination, transform.position) < 2)
            {
                actualTarget++;

                if (actualTarget >= targets.Length)
                {
                    actualTarget = 0;
                }
            }
        }
        else if (actualState == state.Chase)
        {
            transform.Translate(Vector3.forward * 5 * Time.deltaTime);
        }



        if(agent.remainingDistance < agent.stoppingDistance)
        {
            anim.SetBool("isMoving", false);
        }
        else
        {
            anim.SetBool("isMoving", true);
        }
    }



}